﻿using SMS_Online.Data;
using SMS_Online.Models;

namespace SMS_Online.Repository
{
    public interface IServiceRepository : IBaseRepository<Service>
    {
        
    }
    public class ServiceRepository : BaseRepository<Service>, IServiceRepository
    {
        public ServiceRepository(ApplicationDbContext context) : base(context)
        {
            
        }

    }
}