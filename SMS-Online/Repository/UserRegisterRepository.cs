﻿using SMS_Online.Data;
using SMS_Online.Models;

namespace SMS_Online.Repository
{
    public interface IUserRegisterRepository : IBaseRepository<UserRegister>
    {

    }
    public class UserRegisterRepository : BaseRepository<UserRegister>, IUserRegisterRepository
    {
        public UserRegisterRepository(ApplicationDbContext context) : base(context)
        {

        }

    }
}