﻿using System.ComponentModel.DataAnnotations;

namespace SMS_Online.Models
{
    public class Messages : Base
    {
        [Required]
        public string ToPhoneNumber { get; set; }

        [Required]
        public string MessageText { get; set; }

    }
}
