﻿
namespace SMS_Online.Models
{
    public class Service : Base
    {
        public string? ServiceName { get; set; } = "";
        public string? Description { get; set; } = "";
        public decimal? Price { get; set; } = 0;
    }
}
