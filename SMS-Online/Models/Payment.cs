﻿using System.Data.SqlTypes;

namespace SMS_Online.Models
{
    public class Payment : Base
    {
        public int? UserId { get; set; } = 0;
        public decimal? Amount { get; set; } = 0;
        public DateTime? Payment_Date { get; set; }
    }
}
