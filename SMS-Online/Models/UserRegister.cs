﻿namespace SMS_Online.Models
{
    public class UserRegister : Base
    {
        public string Username { get; set; } = "";
        public string Password { get; set; } = "";
        public string ConfirmPassword { get; set; } = "";
        public string MobileNumber { get; set; } = "";
    }

}
