﻿namespace SMS_Online.Models
{
    public class UserDetail 
    {
        public string Address { get; set; } = "";
        public string Email { get; set; } = "";
        public string First_Name { get; set; } = "";
        public string Last_Name { get; set; } = "";
        public DateTime? Dob { get; set; }
        public string Hobbies { get; set; } = "";
        public string Likes { get; set; } = "";
        public string Dislikes { get; set; } = "";
        public string Cuisines { get; set; } = "";
        public string Sport { get; set; } = "";
        public string Profile_Photo { get; set; } = "";
        public string Qualification { get; set; } = "";
        public string School { get; set; } = "";
        public string College { get; set; } = "";
        public string Organization { get; set; } = "";
        public string Designation { get; set; } = "";
        public DateTime? Registration_Date { get; set; }

    }
}
