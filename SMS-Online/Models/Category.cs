﻿namespace SMS_Online.Models
{
    public class Category : Base
    {
        public string Title { get; set; } = "";
        public string Description { get; set; } = "";
        public int Position { get; set; } = 0;
        public DateTime? Create_Date { get; set; }
        public string Create_By { get; set; } = "";
        public DateTime? Modified_Date { get; set; }
        public string Modified_By { get; set; } = "";

    }
}
