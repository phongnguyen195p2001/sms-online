﻿namespace SMS_Online.Models
{
    public class Friend : Base
    {
        public int UserId { get; set; } = 0;
        public int FriendId { get; set; } = 0;
        public string Status { get; set; } = "";
    }
}
