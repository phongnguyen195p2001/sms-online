﻿namespace SMS_Online.Models
{
    public class News : Base
    {
        public int CategoryId { get; set; } = 0;
        public string Title { get; set; } = "";
        public string Description { get; set; } = "";
        public string Detail { get; set; } = "";
        public string Image { get; set; } = "";
        public DateTime? Create_Date { get; set; }
        public string Create_By { get; set; } = "";
        public DateTime? Modified_Date { get; set; }
        public string Modified_By { get; set; } = "";
    }
}
