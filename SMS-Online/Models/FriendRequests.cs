﻿ namespace SMS_Online.Models
{
    public class FriendRequests : Base
    {
        public int SenderId { get; set; } = 0;
        public int ReceiverId { get; set; } = 0;
        public string Status { get; set; } = "";
    }
}
