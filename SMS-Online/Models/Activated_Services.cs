﻿namespace SMS_Online.Models
{
    public class Activated_Services : Base
    {
        public int UserId { get; set; } = 0;
        public int ServiceId { get; set; } = 0;
        public DateTime? Start_Date { get; set; }
        public DateTime? End_Date { get; set; }

    }
}
