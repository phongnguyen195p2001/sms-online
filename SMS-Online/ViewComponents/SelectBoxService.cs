﻿using SMS_Online.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Query.Internal;


namespace SMS_Online.ViewComponents
{
    public class SelectBoxService : ViewComponent
    {
        private ApplicationDbContext _context;
        public SelectBoxService(ApplicationDbContext context)
        {
            _context = context;
        }
        public IViewComponentResult Invoke(string cssClass)
        {
            // Xử lý logic và trả về một đối tượng Model cho view
            ViewBag.CSSClass = cssClass;
            var model = _context.Service.ToList(); // Xử lý logic để tạo model

            return View(model); // Trả về view và truyền model vào view
        }
    }
}

