﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using SMS_Online.Models;

namespace SMS_Online.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
            DataSeeding.SeedingPostalService(this);
        }
        public virtual DbSet<User> User { get; set; }
        public virtual DbSet<Service> Service { get; set; }
        public virtual DbSet<Payment> Payment { get; set; }
        public virtual DbSet<News> News { get; set; }
        public virtual DbSet<Messages> Messages { get; set; }
        public virtual DbSet<FriendRequests> FriendRequests { get; set; }
        public virtual DbSet<Friend> Friend { get; set; }
        public virtual DbSet<Category> Category { get; set; }
        public virtual DbSet<Activated_Services> Activated_Services { get; set; }
        
    }
}