// SmsController.cs
using Microsoft.AspNetCore.Mvc;
using SMS_Online.Models;
using Twilio;
using Twilio.Rest.Api.V2010.Account;

public class SmsController : Controller
{
    // Thông tin cấu hình Twilio (thay thế bằng thông tin của bạn)
    private const string AccountSid = "AC239349b770c31c48a3e3e6b862e5445a";
    private const string AuthToken = "0acebd852569c2842b61db55dcf2f972";
    private const string TwilioPhoneNumber = "+14706195064";

    // GET: /Sms
    [HttpGet]
    public IActionResult Index()
    {
        return View();
    }

    // POST: /Sms/Send
    [HttpPost]
    public IActionResult Send(Messages model)
    {
        if (!ModelState.IsValid)
        {
            return View("Index", model);
        }

        // Gửi tin nhắn bằng Twilio
        TwilioClient.Init(AccountSid, AuthToken);

        var message = MessageResource.Create(
            body: model.MessageText,
            from: new Twilio.Types.PhoneNumber(TwilioPhoneNumber),
            to: new Twilio.Types.PhoneNumber(model.ToPhoneNumber)
        );

        ViewBag.ResultMessage = "Tin nhắn đã được gửi thành công đến số điện thoại " + model.ToPhoneNumber;

        return View("Index");
    }
}
