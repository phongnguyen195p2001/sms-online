﻿using Microsoft.AspNetCore.Mvc;
using SMS_Online.Data;
using SMS_Online.Models;
using SMS_Online.Repository;

namespace SMS_Online.Controllers
{
    public class ServiceController : BaseController<Service>
    {
        private IServiceRepository _ServiceRepository;

        public ServiceController(IServiceRepository ServiceRepository, ApplicationDbContext applicationDbContext, IBaseRepository<Service> baseRepository)
            : base(baseRepository, applicationDbContext)
        {
            _ServiceRepository = ServiceRepository;

        }        
        /*public IActionResult Index()
        {

            return View();
        }*/

    }

}
