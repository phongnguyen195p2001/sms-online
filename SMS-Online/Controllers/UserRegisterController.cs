﻿using Microsoft.AspNetCore.Mvc;
using SMS_Online.Data;
using SMS_Online.Models;
using SMS_Online.Repository;

namespace SMS_Online.Controllers
{
    public class UserRegisterController : BaseController<UserRegister>
    {
        private IUserRegisterRepository _UserRegisterRepository;

        public UserRegisterController(IUserRegisterRepository UserRegisterRepository, ApplicationDbContext applicationDbContext, IBaseRepository<UserRegister> baseRepository)
            : base(baseRepository, applicationDbContext)
        {
            _UserRegisterRepository = UserRegisterRepository;

        }
        /*public IActionResult Index()
        {
            return View();
        }*/
        private static List<UserRegister> registeredUsers = new List<UserRegister>();


        [HttpGet]
        public IActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Register(UserRegister model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }


            if (IsUsernameAvailable(model.Username))
            {

                registeredUsers.Add(model);
                return RedirectToAction("RegistrationSuccessful");
            }
            else
            {
                ModelState.AddModelError("Username", "Tên người dùng đã được sử dụng.");
                return View(model);
            }
        }

        [HttpGet]
        public IActionResult RegistrationSuccessful()
        {
            return View();
        }


        private bool IsUsernameAvailable(string username)
        {
            foreach (var user in registeredUsers)
            {
                if (user.Username.Equals(username))
                {
                    return false;
                }
            }
            return true;
        }
    }
}